#Nose tests for acp_times.py

import nose
import logging
import acp_times
from acp_times import open_time
from acp_times import close_time
"""
Each assertion comparison was found using the online calculator at: https://rusa.org/octime_acp.html
"""

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

#Format used: YYYY-MM-DDTHH:mm:ss.SZ
#ISO8601 format: YYYY-MM-DDTHH:mm:ss+00:00

"""
Nose tests for open_time method
"""

def test_open_zero():
    #Test what happens at value 0
    assert open_time(0, 200, "2017-01-01T10:00:00.0Z") == "2017-01-01T10:00:00+00:00" 

def test_open_before_200():
    #Test what happens from in a control point between 0 and 200
    assert open_time(10, 200, "2017-01-01T10:00:00.0Z") == "2017-01-01T10:18:00+00:00"

def test_open_after_200():
    #Test what happens if a value OVER 200 is entered
    assert open_time(210, 200, "2017-01-01T10:00:00.0Z") == "2017-01-01T15:53:00+00:00"


"""
#Nose tests for close_time method
"""

def test_close_zero():
    #Test what happens at value 0
    assert close_time(0, 200, "2017-01-01T10:00:00.0Z") == "2017-01-01T11:00:00+00:00" 

def test_close_before_200():
    #Test what happens from in a control point between 0 and 200
    assert close_time(10, 200, "2017-01-01T10:00:00.0Z") == "2017-01-01T11:30:00+00:00"

def test_close_after_200():
    #Test what happens if a value OVER 200 is entered
    assert close_time(210, 200, "2017-01-01T10:00:00.0Z") == "2017-01-01T23:30:00+00:00"

